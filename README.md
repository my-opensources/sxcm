# STARTX SXCM Ansible collection

The purpose of this collection is to manage the lifecycle of multi-OKD/OCP clusters
using a full infrastructure stack build around automation controller, terraform, vault and gitlab.

Refer to [latest documentation for the `startxfr.sxcm` ansible collection](https://startx-ansible-sxcm.readthedocs.io)
for more informations on how to configure and use this top level role.

## Availables roles

- [infra role](https://startx-ansible-sxcm.readthedocs.io/en/latest/roles/infra)
- [world role](https://startx-ansible-sxcm.readthedocs.io/en/latest/roles/world)
- [repository role](https://startx-ansible-sxcm.readthedocs.io/en/latest/roles/repository)
- [git role](https://startx-ansible-sxcm.readthedocs.io/en/latest/roles/git)
- [cluster role](https://startx-ansible-sxcm.readthedocs.io/en/latest/roles/cluster)
- [clusterservice role](https://startx-ansible-sxcm.readthedocs.io/en/latest/roles/clusterservice)

## History

Full history for this collection is available on the [startx sxcm collection history](https://startx-ansible-sxcm.readthedocs.io/en/latest/history) page.
