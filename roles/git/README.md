# StartXClusterManager : Working-copy role

Read the [startxfr.sxcm.workingcopy role documentation](https://startx-ansible-sxcm.readthedocs.io/en/latest/roles/workingcopy/)
for more information on how to use [STARTX sxcm ansible collection](https://galaxy.ansible.com/startxfr/sxcm) workingcopy role.
