# StartXClusterManager : Cluster role

Read the [startxfr.sxcm.cluster role documentation](https://startx-ansible-sxcm.readthedocs.io/en/latest/roles/cluster/)
for more information on how to use [STARTX sxcm ansible collection](https://galaxy.ansible.com/startxfr/sxcm) cluster role.
