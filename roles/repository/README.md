# StartXClusterManager : Repository role

Read the [startxfr.sxcm.repository role documentation](https://startx-ansible-sxcm.readthedocs.io/en/latest/roles/repository/)
for more information on how to use [STARTX sxcm ansible collection](https://galaxy.ansible.com/startxfr/sxcm) repository role.
