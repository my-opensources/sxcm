# StartXClusterManager : Infrastructure role

Read the [startxfr.sxcm.infra role documentation](https://startx-ansible-sxcm.readthedocs.io/en/latest/roles/infra/)
for more information on how to use [STARTX sxcm ansible collection](https://galaxy.ansible.com/startxfr/sxcm) infra role.
