# StartXClusterManager : World role

Read the [startxfr.sxcm.world role documentation](https://startx-ansible-sxcm.readthedocs.io/en/latest/roles/world/)
for more information on how to use [STARTX sxcm ansible collection](https://galaxy.ansible.com/startxfr/sxcm) world role.
