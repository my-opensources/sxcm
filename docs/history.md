# Release history

## Version 0.3

*Upgrade collections for AAP 2.2 compat*

## Version 0.2

| version | Date     | Description                                        |
| ------- | -------- | -------------------------------------------------- |
| 0.2.0   | 22-05-22 | unstable version 0.2.0 used to align versions      |
| 0.2.1   | 22-05-22 | stable version 0.2.1                               |
| 0.2.9   | 22-07-24 | stable version 0.2.9 preparing 0.3.0 release       |
| 0.2.10  | 22-07-24 | stable version 0.2.10                              |
| 0.2.11  | 22-07-24 | stable version 0.2.11 vith CI/CD release toolchain |
| 0.2.12  | 22-08-31 | stable version 0.2.12 vith CI/CD release toolchain |
| 0.2.91  | 23-01-30 | prepare 0.3 release                                |

## Version 0.1

| version | Date     | Description                                             |
| ------- | -------- | ------------------------------------------------------- |
| 0.1.0   | 22-05-08 | stable version 0.1.0                                    |
| 0.1.1   | 22-05-09 | add collection playook examples                         |
| 0.1.3   | 22-05-12 | stable repository                                       |
| 0.1.4   | 22-05-13 | improve isolation and interface for workingcopy role    |
| 0.1.5   | 22-05-17 | add cluster-service role and improve action for cluster |
| 0.1.6   | 22-05-22 | Improve documentation                                   |
| 0.1.8   | 22-05-22 | debug documentation generation                          |

## Version 0.0

| version | Date     | Description                                            |
| ------- | -------- | ------------------------------------------------------ |
| 0.0.1   | 22-05-07 | Initial release for public audience                    |
| 0.0.2   | 22-05-08 | First release with a stable interface                  |
| 0.0.3   | 22-05-08 | Improve documentation                                  |
| 0.0.27  | 22-05-08 | Improve doc and stablilizing release                   |
| 0.0.29  | 22-05-08 | Align all startfr collection release to version 0.0.29 |
