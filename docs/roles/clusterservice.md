# SXCM : Cluster Service role

The purpose of this role is to manage openshift cluster service state under a git repository.
Allow users to enable, disable, associate and dissociate a cluster-service on a cluster.

This include enable, associate and dissociate and the destructive couterpart (disable, dissociate) of kubernetes resources
bundled as a single unit of deployment named cluster-service.

This role is part of the [STARTX sxcm ansible collection](https://galaxy.ansible.com/startxfr/sxcm).

## Requirements

- Ansible runtime
- Installation of the requirements with `ansible-galaxy install -r meta/requirements.yml`
- This depend on the [startxfr.sxcm.cluster role](https://startx-ansible-sxcm.readthedocs.io/en/latest/roles/cluster/) context. If not already executed into your playbook, this role wil be initialize.

## Role Variables

| Key                     | Default                        | Description                                                                   |
| ----------------------- | ------------------------------ | ----------------------------------------------------------------------------- |
| sx_cs_action            | none                           | The action to perform (could be enable, disable, associate, dissociate, info) |
| sx_cs_cluster_name      | `sx_cluster_name`              | Name of the concerned cluster (mandatory)                                     |
| sx_cs_name              | dummy                          | Name of the concerned clusterservice (mandatory)                              |
| sx_cs_dirname           | cluster-services               | directory name used to store the cluster services definition.                 |
| sx_cs_path              | `sx_cluster_path`              | directory used to store the cluster definition and git managed.               |
| sx_cs_cache_manifest    | ""                             | Variable used to load the content of the <cluster-service> manifest           |
| sx_cs_exist             | false                          | gitops clusterservice configuration exist (boolean)                           |
| sx_cs_remote_name       | `sx_cluster_remote_name`       | remote repository name in local copy                                          |
| sx_cs_local_branch      | `sx_cluster_local_branch`      | local branch (linked to the sx_cluster_local_branch)                          |
| sx_cs_remote_name       | `sx_cluster_remote_name`       | remote repository name in local copy                                          |
| sx_cs_info.mode         | log                            | how to display information (file or log)                                      |
| sx_cs_info.suffix       | info-clusterservice.txt        | Destination suffix for the cluster-service informations filename              |
| sx_cs_info.dest         | /tmp                           | Destination directory for the cluster-service informations                    |
| sx_cs_ocp_distro        | `sx_cluster_ocp_distro`        | Kind of ocp distribution (okd or ocp)                                         |
| sx_cs_ocp_release       | `sx_cluster_ocp_release`       | Release version to use for CLI                                                |
| sx_cs_ocp_release_minor | `sx_cluster_ocp_release_minor` | minor release (used whn okd distro)                                           |
| sx_cs_ocp_bin_directory | `sx_cluster_ocp_bin_directory` | Directory where binary are installed                                          |

## Dependencies

- `ansible.builtin` collection
- `startxfr.client` collection. See [startx client collection documentation](https://galaxy.ansible.com/startxfr/client)
- `startxfr.sxcm` collection. See [startx sxcm collection documentation](https://galaxy.ansible.com/startxfr/sxcm)

## Example playbooks

### Associate dummy cluster-service playbook

Associate the dummy cluster-service to the current sxcm cluster.

```yaml
- name: Associate dummy cluster-service
  hosts: localhost
  roles:
    - role: startxfr.sxcm.cluster
      vars:
        sx_cluster_action: "info"
        sx_cluster_name: "mycluster"
    - role: startxfr.sxcm.clusterservice
      vars:
        sx_cs_action: "associate"
        sx_cs_name: "dummy"
```

### Enable dummy cluster-service playbook

Enable the dummy cluster-service to the current sxcm cluster.

```yaml
- name: Enable dummy cluster-service
  hosts: localhost
  roles:
    - role: startxfr.sxcm.cluster
      vars:
        sx_cluster_action: "info"
        sx_cluster_name: "mycluster"
    - role: startxfr.sxcm.clusterservice
      vars:
        sx_cs_action: "enable"
        sx_cs_name: "dummy"
```

### Information on cluster-service playbook

Read informations about the dummy cluster-service into the current sxcm cluster.

```yaml
- name: Information on dummy cluster-service
  hosts: localhost
  roles:
    - role: startxfr.sxcm.cluster
      vars:
        sx_cluster_action: "info"
        sx_cluster_name: "mycluster"
    - role: startxfr.sxcm.clusterservice
      vars:
        sx_cs_action: "info"
        sx_cs_name: "dummy"
```

### Disable dummy cluster-service playbook

Disable the dummy cluster-service to the current sxcm cluster.

```yaml
- name: Disable dummy cluster-service
  hosts: localhost
  roles:
    - role: startxfr.sxcm.cluster
      vars:
        sx_cluster_action: "info"
        sx_cluster_name: "mycluster"
    - role: startxfr.sxcm.clusterservice
      vars:
        sx_cs_action: "disable"
        sx_cs_name: "dummy"
```
