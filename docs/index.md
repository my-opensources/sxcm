# STARTX sxcm Ansible collection

This is the main documentation for the `startxfr.sxcm` ansible collection.

## Roles

| version                                | Description                                   |
| -------------------------------------- | --------------------------------------------- |
| [infra role](roles/cluster/)           | Install or uninstall sxcm infrastructure      |
| [world role](roles/world/)             | Install or uninstall sxcm world               |
| [repository role](roles/repository/)   | Install or uninstall sxcm cluster repository  |
| [workingcopy role](roles/workingcopy/) | Install or uninstall sxcm cluster workingcopy |
| [cluster role](roles/cluster/)         | Manage sxcm cluster lifecycle                 |
| [clusterservice role](roles/cluster/)  | Enable or disable sxcm cluster service        |

## History

If you want to follow the collection history, you can follow the [release history](history.md).

## Improve and develop role

If you want to contribute to the startx inititive you can follow the [developper guidelines](developpers.md).

## Contributors

A full list of the contributors is available under the [contributors list page](contributors.md).
